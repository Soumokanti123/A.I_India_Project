# A.I_India_Project
We present a traffic video analysis system based on computer vision techniques.The system is designed to automatically gather important statistics for policy makers and regulators in an automated fashion. These statistics include vehicle counting, vehicle type classification, estimation of vehicle speed from video and lane usage monitoring. 
